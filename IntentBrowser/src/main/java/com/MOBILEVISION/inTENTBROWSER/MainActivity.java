package com.MOBILEVISION.inTENTBROWSER;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Set;

public class MainActivity extends Activity {

    private static int RESULT_LOAD_IMAGE = 1;
    private static int RESULT_LOAD_CONTACT = 10;
    private static final int PICK_FROM_GALLERY = 2;
    Bitmap thumbnail = null;
    String picPath = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread t, Throwable e) {
                try {

                    PrintWriter pw = new PrintWriter(new OutputStreamWriter(openFileOutput(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_DCIM) + "possible.txt", 0)));
                    e.printStackTrace(pw);
                    pw.flush();
                    pw.close();
                } catch (FileNotFoundException e1) {
                    // do nothing
                }

                MainActivity.this.finish();
            }
        });
    }

    public void ButtonOnClick(View v) {
        switch (v.getId()) {
            case R.id.uiSelectPicture:

                SelectPicture();

                break;
            case R.id.uiSelectContact:

                SelectContact();

                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void SelectPicture() {
        Intent in = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(in, RESULT_LOAD_IMAGE);
    }

    public void SelectContact() {
        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(contactPickerIntent, RESULT_LOAD_CONTACT);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data){

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            thumbnail = (BitmapFactory.decodeFile(picturePath));

            picPath = picturePath;

            // Display the image
            ImageView iv = (ImageView) findViewById(R.id.imageView);

            iv.setImageBitmap(thumbnail);
            iv.invalidate();

            // Show Image path
            TextView tv = (TextView) findViewById(R.id.uiResult);
            tv.setText(picPath);
        }
        else if (requestCode == RESULT_LOAD_CONTACT && resultCode == RESULT_OK && null != data) {

            ShowMessageBox();
        }
    }

    private void ShowMessageBox() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Mobile Vision IntentBrowser")
                .setMessage("Feature still to be implemented...")
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //dismiss the dialog
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
